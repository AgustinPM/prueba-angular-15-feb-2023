export interface Superhero {
  id: number | null;
  name: string;
  place?: string;
  realName?: string;
  job?: string;
}
