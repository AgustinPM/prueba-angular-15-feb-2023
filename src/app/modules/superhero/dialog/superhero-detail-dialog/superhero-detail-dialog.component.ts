import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Superhero } from '../../models/superhero.model';

@Component({
  selector: 'app-superhero-detail-dialog',
  templateUrl: './superhero-detail-dialog.component.html',
  styleUrls: ['./superhero-detail-dialog.component.scss'],
})
export class SuperheroDetailDialogComponent implements OnInit {
  fg!: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<SuperheroDetailDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Superhero
  ) {}

  ngOnInit(): void {
    this.fg = this.buildFormGroup();
    this.fg.patchValue(this.data);
  }

  /**
   * Build and returns formgroup
   * @returns FormGroup
   */
  private buildFormGroup(): FormGroup {
    return new FormGroup({
      name: new FormControl(null, [Validators.required]),
      place: new FormControl(null),
      realName: new FormControl(null),
      job: new FormControl(null),
    });
  }

  /**
   * Returns form control from form control name
   * @param controlName Form control name
   * @returns FormControl
   */
  getFormControl(controlName: string): FormControl {
    return this.fg.get(controlName) as FormControl;
  }

  /**
   * Returns form control value from form control name
   * @param controlName Form control name
   * @returns any value
   */
  getFormValue(controlName: string): any {
    return this.fg.get(controlName)?.value;
  }

  /**
   * Returns formgroup value
   * @returns Superhero
   */
  getFormGroupValue(): Superhero {
    this.data = { ...this.data, ...this.fg.value };
    return this.fg.dirty ? this.data : ({} as Superhero);
  }

  /**
   * Clear form control value
   * @param controlName Form control name
   */
  clearFormValue(controlName: string): void {
    this.fg.get(controlName)?.reset();
  }
}
