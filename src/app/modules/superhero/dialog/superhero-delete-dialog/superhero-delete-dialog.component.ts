import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Superhero } from '../../models/superhero.model';

@Component({
  selector: 'app-superhero-delete-dialog',
  templateUrl: './superhero-delete-dialog.component.html',
  styleUrls: ['./superhero-delete-dialog.component.scss'],
})
export class SuperheroDeleteDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<SuperheroDeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Superhero
  ) {}
}
