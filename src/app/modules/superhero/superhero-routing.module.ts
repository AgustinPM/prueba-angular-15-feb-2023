import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DirtyGuard } from 'src/app/core/guards/dirty.guard';
import { SuperheroDetailComponent } from './pages/superhero-detail/superhero-detail.component';
import { SuperheroListBackendComponent } from './pages/superhero-list-backend/superhero-list-backend.component';
import { SuperheroListComponent } from './pages/superhero-list/superhero-list.component';

const routes: Routes = [
  {
    path: '',
    component: SuperheroListComponent
  },
  {
    path: 'backend',
    component: SuperheroListBackendComponent
  },
  {
    path: 'new',
    component: SuperheroDetailComponent,
    canDeactivate: [DirtyGuard]
  },
  {
    path: ':id',
    component: SuperheroDetailComponent,
    canDeactivate: [DirtyGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuperheroRoutingModule {}
