import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Superhero } from '../models/superhero.model';

@Injectable()
export class SuperheroApiService {
  private readonly apiUrl = `${environment.apiUrl}/superhero`;

  constructor(protected http: HttpClient) {}

  /**
   * Get all superheroes
   * @returns Array of Superheroes
   */
  getAll(): Observable<Superhero[]> {
    return this.http.get<Superhero[]>(this.apiUrl);
  }

  /**
   * Get paginated superhero
   * @param page Index of page to search (default: 1)
   * @param limit Limit per page to search (default: 4)
   * @returns Page of superhero
   */
  getPaginated(
    page: number = 1,
    limit: number = 5,
    filter?: string
  ): Observable<HttpResponse<any>> {
    // - ?name_like=man
    return this.http.get(
      `${this.apiUrl}?_page=${page}&_limit=${limit}&${filter}`,
      {
        observe: 'response',
      }
    );
  }

  /**
   * Get superhero by id
   * @param id Superhero id
   * @returns Superhero
   */
  getById(id: number): Observable<Superhero> {
    return this.http.get<Superhero>(`${this.apiUrl}/${id}`);
  }

  /**
   * Get superhero by array of ids
   * @param ids Array superhero id
   * @returns Return array of superhero
   */
  getByIds(ids: number[]): Observable<Superhero[]> {
    let idParam = '';
    ids.forEach((id: number, i: number) => {
      if (i != 0) {
        idParam += '&';
      }
      idParam += `id=${id}`;
    });
    return this.http.get<Superhero[]>(`${this.apiUrl}?${idParam}`);
  }

  /**
   * Save a superhero
   * @param superhero superhero object
   * @returns Updated superhero object
   */
  save(superhero: Superhero): Observable<Superhero> {
    return superhero.id ? this.update(superhero) : this.create(superhero);
  }

  /**
   * Update a superhero
   * @param superhero superhero object
   * @returns Updated superhero object
   */
  protected update(superhero: Superhero): Observable<Superhero> {
    return this.http.put<Superhero>(
      `${this.apiUrl}/${superhero.id}`,
      superhero
    );
  }

  /**
   * Create a superhero
   * @param superhero superhero object
   * @returns Created superhero object
   */
  protected create(superhero: Superhero): Observable<Superhero> {
    return this.http.post<Superhero>(`${this.apiUrl}`, superhero);
  }

  /**
   * Delete a superhero
   * @param id superhero id
   */
  delete(id: number): Observable<Superhero> {
    return this.http.delete<Superhero>(`${this.apiUrl}/${id}`);
  }
}
