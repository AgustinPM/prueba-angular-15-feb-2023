import { HttpClientModule } from '@angular/common/http';
import { async, TestBed } from '@angular/core/testing';
import { Superhero } from '../models/superhero.model';
import { SuperheroApiService } from './superhero-api.service';

describe('SuperheroApiService', () => {
  let service: SuperheroApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [SuperheroApiService],
    });
    service = TestBed.inject(SuperheroApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get all superheroes', async((done: () => void) => {
    service.getAll().subscribe((superheroes) => {
      expect(superheroes.length).toBeGreaterThan(0);
    });
  }));

  it('should get paginated superheroes', async((done: () => void) => {
    service.getPaginated(1, 5).subscribe((response) => {
      expect(response.body.length).toBeGreaterThan(0);
    });
  }));

  it('should get paginated superheroes with filter name man', async((
    done: () => void
  ) => {
    service.getPaginated(1, 5, 'man').subscribe((response) => {
      expect(response.body.length).toBeGreaterThan(0);
    });
  }));

  it('should get superhero by id', async((done: () => void) => {
    service.getById(1).subscribe((superhero) => {
      expect(superhero.id).toBe(1);
    });
  }));

  it('should get superhero by ids', async((done: () => void) => {
    service.getByIds([1, 2]).subscribe((superheroes) => {
      expect(superheroes.length).toBeGreaterThan(0);
    });
  }));

  it('should create superhero', async((done: () => void) => {
    service.save({ name: 'test' } as Superhero).subscribe((superhero) => {
      expect(superhero).toBeTruthy();
    });
  }));

  it('should edit superhero 5', async((done: () => void) => {
    service.getById(5).subscribe((superhero) => {
      superhero.name = 'test';
      service.save(superhero).subscribe((superhero) => {
        expect(superhero).toBeTruthy();
      });
    });
  }));

  it('should delete superhero 7', async((done: () => void) => {
    service.delete(7).subscribe((response) => {
      expect(response).toBeTruthy();
    });
  }));
});
