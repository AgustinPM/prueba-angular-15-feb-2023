import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Superhero } from '../models/superhero.model';

@Injectable()
export class SuperheroService {
  superHeroList = new BehaviorSubject<Superhero[]>([]);

  constructor() {}

  // Getter
  getSuperHeroList(): Observable<Superhero[]> {
    return this.superHeroList.asObservable();
  }

  // Setter
  setSuperHeroList(superHeroList: Superhero[]): void {
    this.superHeroList.next(superHeroList);
  }
}
