import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { catchError, iif, mergeMap, of, tap, throwError } from 'rxjs';
import { SnackbarService } from 'src/app/core/services/snackbar.service';
import { SuperheroDeleteDialogComponent } from '../../dialog/superhero-delete-dialog/superhero-delete-dialog.component';
import { SuperheroDetailDialogComponent } from '../../dialog/superhero-detail-dialog/superhero-detail-dialog.component';
import { Superhero } from '../../models/superhero.model';
import { SuperheroApiService } from '../../services/superhero-api.service';

@Component({
  selector: 'app-superhero-list-backend',
  templateUrl: './superhero-list-backend.component.html',
  styleUrls: ['./superhero-list-backend.component.scss'],
})
export class SuperheroListBackendComponent implements OnInit, AfterViewInit {
  //Table Data
  displayedColumns: string[] = [
    'id',
    'name',
    'place',
    'realName',
    'job',
    'actions',
  ];
  dataSource = new MatTableDataSource<Superhero>([]);

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private shApiService: SuperheroApiService,
    private snackbar: SnackbarService,
    private dialog: MatDialog,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loadHeroPage();
  }

  ngAfterViewInit(): void {
    this.paginator.page.subscribe((_) => this.loadHeroPage());
  }

  /**
   * Superhero endpoint
   */
  loadHeroPage(filterValue = ''): void {
    this.shApiService
      .getPaginated(
        this.paginator?.pageIndex + 1,
        this.paginator?.pageSize,
        filterValue
      )
      .pipe(
        tap((res) => {
          this.paginator.length = res.headers.get('X-Total-Count') as any;
          this.dataSource.data = res.body;
        }),
        catchError((err) => {
          return throwError(err);
        })
      )
      .subscribe();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.paginator.pageIndex = 0;
    this.loadHeroPage(`name_like=${filterValue}`);
  }

  btnAdd(): void {
    this.btnEdit({ id: null } as Superhero);
  }

  btnEdit(row: Superhero) {
    const dialogRef = this.dialog
      .open(SuperheroDetailDialogComponent, {
        data: row,
      })
      .afterClosed()
      .pipe(
        mergeMap((res: Superhero) =>
          iif(
            () => Object.keys(res).length !== 0,
            this.shApiService.save(res),
            of()
          )
        ),
        tap((res: Superhero) => {
          this.loadHeroPage();
          this.snackbar.openSnackBar(`${res.name} superhero edited.`);
        })
      )
      .subscribe();
  }

  btnDelete(row: Superhero): void {
    this.dialog
      .open(SuperheroDeleteDialogComponent, {
        data: row,
      })
      .afterClosed()
      .pipe(
        mergeMap((confirm) =>
          iif(() => confirm, this.shApiService.delete(Number(row.id)), of())
        ),
        tap((res) => {
          this.loadHeroPage();
          this.snackbar.openSnackBar(`${row.name} superhero deleted.`);
        })
      )
      .subscribe();
  }
}
