import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { AppModule } from 'src/app/app.module';
import { Superhero } from '../../models/superhero.model';
import { SuperheroModule } from '../../superhero.module';

import { SuperheroListComponent } from './superhero-list.component';

describe('SuperheroListComponent', () => {
  let component: SuperheroListComponent;
  let fixture: ComponentFixture<SuperheroListComponent>;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SuperheroListComponent],
      imports: [SuperheroModule, AppModule],
    }).compileComponents();

    fixture = TestBed.createComponent(SuperheroListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router = TestBed.inject(Router);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to detail page', () => {
    const spy = spyOn(router, 'navigate');
    component.btnEdit({ id: 1 } as Superhero);
    expect(spy).toHaveBeenCalledWith(['superhero', 1]);
  });

  it('should navigate to create page', () => {
    const spy = spyOn(router, 'navigate');
    component.btnAdd();
    expect(spy).toHaveBeenCalledWith(['superhero', 'new']);
  });
});
