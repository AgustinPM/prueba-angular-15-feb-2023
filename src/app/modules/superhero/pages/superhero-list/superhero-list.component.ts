import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import {
  catchError,
  iif,
  mergeMap,
  Observable,
  of,
  tap,
  throwError
} from 'rxjs';
import { SnackbarService } from 'src/app/core/services/snackbar.service';
import { SuperheroDeleteDialogComponent } from '../../dialog/superhero-delete-dialog/superhero-delete-dialog.component';
import { Superhero } from '../../models/superhero.model';
import { SuperheroApiService } from '../../services/superhero-api.service';
import { SuperheroService } from '../../services/superhero.service';

@Component({
  selector: 'app-superhero-list',
  templateUrl: './superhero-list.component.html',
  styleUrls: ['./superhero-list.component.scss'],
})
export class SuperheroListComponent implements OnInit, AfterViewInit {
  superheroes$!: Observable<Superhero[]>;

  //Table Data
  displayedColumns: string[] = [
    'id',
    'name',
    'place',
    'realName',
    'job',
    'actions',
  ];
  dataSource = new MatTableDataSource<Superhero>([]);

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private shService: SuperheroService,
    private shApiService: SuperheroApiService,
    private dialog: MatDialog,
    private snackbar: SnackbarService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.dataSource.filterPredicate = (data, filter) => {
      return data.name.toLowerCase().includes(filter);
    };

    this.loadHeroPage(); // Initial Value

    // Subscribe to Superhero Service Value (in HTML we use async pipe)
    // if we use Superhero Service Value
    this.superheroes$ = this.shService
      .getSuperHeroList()
      .pipe(tap((res) => (this.dataSource.data = res)));
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  /**
   * Superhero endpoint
   */
  loadHeroPage(): void {
    this.shApiService
      .getAll()
      .pipe(
        tap((res) => {
          //this.dataSource.data = res; // This line is if we don't use Superhero Service Value
          this.shService.setSuperHeroList(res);
        }),
        catchError((err) => {
          this.snackbar.openSnackBar('Error loading data');
          return throwError(err);
        })
      )
      .subscribe();
  }

  /**
   * Filter table data
   * @param event Event
   */
  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  // Button Add action
  btnAdd(): void {
    this.router.navigate(['superhero', 'new']);
  }

  /**
   * Button Edit action
   * @param row Superhero
   */
  btnEdit(row: Superhero): void {
    this.router.navigate(['superhero', row.id]);
  }

  /**
   * Button Delete action
   * @param row Superhero
   */
  btnDelete(row: Superhero): void {
    this.dialog
      .open(SuperheroDeleteDialogComponent, {
        data: row,
      })
      .afterClosed()
      .pipe(
        mergeMap((confirm) =>
          iif(() => confirm, this.shApiService.delete(Number(row.id)), of())
        ),
        tap((res) => {
          this.snackbar.openSnackBar(`${row.name} superhero deleted.`);
          //this.loadHeroPage(); // This line is if we don't use Superhero Service Value

          // This part if we use Superhero Service Value instead api call
          let filteredData = this.shService.superHeroList.value.filter(
            (x) => x.id !== row.id
          );
          this.shService.setSuperHeroList(filteredData);
        })
      )
      .subscribe();
  }
}
