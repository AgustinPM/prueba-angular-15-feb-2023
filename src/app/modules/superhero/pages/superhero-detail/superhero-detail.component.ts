import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import {
  catchError, iif,
  mergeMap,
  Observable,
  of,
  tap,
  throwError
} from 'rxjs';
import { DirtyComponent } from 'src/app/core/guards/dirty.guard';
import { SnackbarService } from 'src/app/core/services/snackbar.service';
import { DialogConfirmComponent } from 'src/app/shared/components/dialog-confirm/dialog-confirm.component';
import { Superhero } from '../../models/superhero.model';
import { SuperheroApiService } from '../../services/superhero-api.service';
import { SuperheroService } from '../../services/superhero.service';

@Component({
  selector: 'app-superhero-detail',
  templateUrl: './superhero-detail.component.html',
  styleUrls: ['./superhero-detail.component.scss'],
})
export class SuperheroDetailComponent implements OnInit, DirtyComponent {
  headerText = 'Superhero Detail';
  isNew = false;
  isEdit = false;
  isDirty = false;
  superhero$!: Observable<Superhero | Superhero[]>;
  superhero!: Superhero;
  fg!: FormGroup;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private shApiService: SuperheroApiService,
    private snackbar: SnackbarService,
    private dialog: MatDialog,
    private shService: SuperheroService
  ) {
    this.fg = this.buildFormGroup();
  }

  ngOnInit(): void {
    this.superhero$ = this.route.params.pipe(
      tap((params) => (this.isEdit = this.isNew = params['id'] ? false : true)),
      mergeMap((params) =>
        iif(
          () => this.isNew,
          of({
            id: null,
            name: '',
            place: '',
            realName: '',
            job: '',
          } as Superhero),
          this.shApiService.getById(params['id'])
        )
      ),
      tap((superhero) => {
        this.fg.patchValue(superhero);
        if (!this.isNew) {
          this.fg.disable();
        }
        this.superhero = superhero;
      })
    );
  }

  // Reactive Forms
  canDeactivate(): boolean {
    return (this.isDirty = this.fg?.dirty);
  }

  // Button edit action
  btnEdit(): void {
    this.isEdit = true;
    this.fg.enable();
  }

  // Button back action
  btnBack(): void {
    this.router.navigate(['..']);
  }

  // Button cancel action
  btnCancel(): void {
    if (!this.fg.dirty) {
      if (this.isNew) {
        this.router.navigate(['..']);
      } else {
        this.isEdit = false;
        this.fg.disable();
      }
    } else {
      this.dialog
        .open(DialogConfirmComponent, {
          data: { message: 'Are you sure you want to cancel?' },
        })
        .afterClosed()
        .subscribe((confirm: Boolean) => {
          if (confirm) {
            if (this.isNew) {
              this.fg.reset(this.superhero);
              this.router.navigate(['..']);
            } else {
              this.isEdit = false;
              this.fg.reset(this.superhero);
              this.fg.disable();
            }
          }
        });
    }
  }

  // Button save action
  btnSave(): void {
    this.fg.markAllAsTouched();

    if (this.fg.invalid) {
      this.snackbar.openSnackBar('Please fill all required fields');
    }
    if (this.fg.valid && this.fg.dirty) {
      let shTemporal = { ...this.superhero, ...this.fg.value };
      this.shApiService
        .save(shTemporal)
        .pipe(
          catchError((error) => {
            this.snackbar.openSnackBar('Save error');
            return throwError(error);
          }),
          tap((superhero: Superhero) => {
            /* To stay in the same detail page
            if (this.isNew) {
              this.isDirty = false;
              this.router.navigate(['superhero', superhero.id]);
            } else {
              this.isEdit = false;
              this.fg.reset(superhero);
              this.fg.disable();
            }
            */
            this.fg.reset(this.superhero);
            this.router.navigate(['..']); // To navigate to the list
            this.snackbar.openSnackBar('Superhero saved');
          })
        )
        .subscribe();
    } else {
      this.btnCancel();
    }
  }

  /**
   * Show clean button in input
   * @param controlName string - control name
   * @returns boolean
   */
  showClean(controlName: string): any {
    return this.fg.get(controlName)?.value && this.isEdit;
  }

  /**
   * Clear form value
   * @param controlName string - control name
   */
  clearFormValue(controlName: string): void {
    this.fg.get(controlName)?.reset();
    this.fg.markAsDirty();
  }

  /**
   * Build and returns formgroup
   * @returns FormGroup
   */
  private buildFormGroup(): FormGroup {
    return new FormGroup({
      name: new FormControl(null, [Validators.required]),
      place: new FormControl(null),
      realName: new FormControl(null),
      job: new FormControl(null),
    });
  }
}
