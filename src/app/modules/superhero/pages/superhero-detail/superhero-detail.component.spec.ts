import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';
import { SuperheroModule } from '../../superhero.module';

import { SuperheroDetailComponent } from './superhero-detail.component';

describe('SuperheroDetailComponent', () => {
  let component: SuperheroDetailComponent;
  let fixture: ComponentFixture<SuperheroDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SuperheroDetailComponent],
      imports: [SuperheroModule, AppModule],
    }).compileComponents();

    fixture = TestBed.createComponent(SuperheroDetailComponent);
    component = fixture.componentInstance;
    //fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should have as title 'Superhero Detail'`, () => {
    const app = fixture.componentInstance;
    expect(app.headerText).toEqual('Superhero Detail');
  });

  it('can edit form after edit button', () => {
    const app = fixture.componentInstance;
    app.btnEdit();
    expect(app.isEdit).toBeTruthy();
  });

  it('can cancel after cancel button', () => {
    const app = fixture.componentInstance;
    app.btnCancel();
    expect(app.isEdit).toBeFalsy();
  })
});
