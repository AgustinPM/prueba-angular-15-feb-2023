import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';

import { SuperheroDeleteDialogComponent } from './dialog/superhero-delete-dialog/superhero-delete-dialog.component';
import { SuperheroDetailDialogComponent } from './dialog/superhero-detail-dialog/superhero-detail-dialog.component';
import { SuperheroDetailComponent } from './pages/superhero-detail/superhero-detail.component';
import { SuperheroListBackendComponent } from './pages/superhero-list-backend/superhero-list-backend.component';
import { SuperheroListComponent } from './pages/superhero-list/superhero-list.component';
import { SuperheroApiService } from './services/superhero-api.service';
import { SuperheroService } from './services/superhero.service';
import { SuperheroRoutingModule } from './superhero-routing.module';


@NgModule({
  declarations: [
    SuperheroListComponent,
    SuperheroListBackendComponent,
    SuperheroDeleteDialogComponent,
    SuperheroDetailDialogComponent,
    SuperheroDetailComponent
  ],
  imports: [
    CommonModule,
    SuperheroRoutingModule,
    SharedModule
  ],
  providers: [SuperheroApiService, SuperheroService]
})
export class SuperheroModule { }
