import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from '../material.module';
import { SnackbarComponent } from './snackbar/snackbar.component';
import { DialogConfirmComponent } from './dialog-confirm/dialog-confirm.component';

@NgModule({
  declarations: [SnackbarComponent, DialogConfirmComponent],
  imports: [CommonModule, MaterialModule],
})
export class ComponentsModule {}
