import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ComponentsModule } from './components/components.module';
import { DirectivesModule } from './directives/directives.module';
import { MaterialModule } from './material.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, MaterialModule, ComponentsModule, DirectivesModule],
  exports: [MaterialModule, ComponentsModule, DirectivesModule],
})
export class SharedModule {}
