import { Directive, HostListener, OnChanges } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[appUppercaseInput]',
})
export class UppercaseInputDirective implements OnChanges {
  constructor(private ngControl: NgControl) {}

  @HostListener('change')
  ngOnChanges(): void {
    const value = this.ngControl.value;
    if (value) {
      this.ngControl.control?.setValue(value.toUpperCase());
    }
  }
}
