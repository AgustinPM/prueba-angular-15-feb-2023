import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UppercaseInputDirective } from './uppercase-input.directive';

@NgModule({
  declarations: [UppercaseInputDirective],
  imports: [CommonModule],
  exports: [UppercaseInputDirective],
})
export class DirectivesModule {}
