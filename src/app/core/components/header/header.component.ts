import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  constructor(private router: Router) {}

  /**
   * Navigate to url
   * @param url string - url to navigate to
   */
  btnClick(url: string): void {
    this.router.navigate([url]);
  }
}
