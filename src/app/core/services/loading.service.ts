import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoadingService {
  isLoading = new Subject<boolean>();

  constructor() {}

  // Show loading component
  show() {
    this.isLoading.next(true);
  }

  // Hide loading component
  hide() {
    this.isLoading.next(false);
  }
}
