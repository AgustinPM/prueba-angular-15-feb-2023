import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarComponent } from 'src/app/shared/components/snackbar/snackbar.component';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {
  constructor(private snackBar: MatSnackBar) {}

  /**
   * Open snackbar
   * @param message string - message to display 
   * @param durationInSeconds number - duration in seconds
   */
  openSnackBar(message: string, durationInSeconds = 5) {
    this.snackBar.openFromComponent(SnackbarComponent, {
      data: { message },
      duration: durationInSeconds * 1000,
      verticalPosition: 'top'
    });
  }
}