import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from '../shared/material.module';
import { SharedModule } from '../shared/shared.module';
import { HeaderComponent } from './components/header/header.component';
import { LoadingComponent } from './components/loading/loading.component';

@NgModule({
  declarations: [HeaderComponent, LoadingComponent],
  exports: [HeaderComponent, LoadingComponent],
  imports: [CommonModule, SharedModule, MaterialModule],
})
export class CoreModule {}
