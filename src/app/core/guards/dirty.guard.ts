import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanDeactivate,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DirtyGuard implements CanDeactivate<DirtyComponent> {
  constructor() {}

  canDeactivate(
    component: DirtyComponent,
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (component.canDeactivate()) {
      return confirm('You have unsaved changes that will be lost.');
    } else {
      return true;
    }
  }
}

export declare interface DirtyComponent {
  canDeactivate: () => boolean | Observable<boolean>;
}
